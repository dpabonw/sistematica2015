## Phylogenetic Reconstruction of Mitocondria using Protein sequences##

For use the scripts you will required the next programs:

- [**Amphora2**](https://github.com/martinwu/AMPHORA2)
- [**Bioperl**](http://www.bioperl.org/wiki/Main_Page)
- [**Python**](https://www.python.org/)
- [**Ipython notebook**](http://ipython.org/notebook.html)
- [**Biopython**](https://www.python.org/)
- [**Bioperl**](http://www.bioperl.org/wiki/Main_Page)
- [**Bash**](https://www.gnu.org/software/bash/)
- [**R**](http://www.r-project.org/)
- [**Phylobayes**](http://megasun.bch.umontreal.ca/People/lartillot/www/download.html)

You need replace `~/Copy/proyecto` for your folder direction i.e. `/`.

In your folder i.e. `/` you need to created `data` folder and inside `processed/` `raw` folders i.e. `mkdir /data/ processed raw`

All process were automated in Bash Scripts.
In Bash folder you can run the Scripts in the next order:

1. Pilot
 1. `./descarga_piloto.sh` (Download genomes for NCBI).
 2. `./rastreo_piloto.sh` (Search genetic markers)
 3. `./recorte_alineamiento.sh` (alignment and trimmer of genetic markers)
